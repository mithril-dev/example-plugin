﻿using MithrilAPI.Debug;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Example_Plugin.MainGUI
{
    public class GUIExample : MonoBehaviour
    {
        // GUIManager is helpful for creating already premade GUI elements.
        GUIManager manager;

        private bool isPopupOpen;

        public void Awake()
        {
            // We need to assign our manager with a GUIManager instance.
            manager = GUIManager.getInstance();

            isPopupOpen = false;
        }

        // Unity 4.3 uses Immediate Mode Graphical User Interface.
        // Since Unity IMGUI is called every frame, this will lag.
        // More GUI = More Lag
        // Unfortunatly, this is unavoidable.
        public void OnGUI()
        {
            // Let's make a box with some text!
            // We need to know where to put the window so we use Rect
            // Let's also make sure to center the window.
            float windowWidth = 400;
            float windowHeight = 200;
            float windowX = (Screen.width - windowWidth) / 2;
            float windowY = (Screen.height - windowHeight) / 2;
            Rect windowRect = new Rect(windowX, windowY, windowWidth, windowHeight);
            // Window rect will be at pos top left of the screen and take up the width and height of the screen.

            // So we can use the manager to draw a window where we specify. Then the title of the window, and can set the
            // background hidden to be false.
            manager.DrawWindow(windowRect, "Example Window", false);

            // IMGUI draws in order. So if we want a button to be on top of our window, we need to put it below it.
            // Again we need a rect location
            float buttonWidth = 200;
            float buttonHeight = 32;
            float buttonX = (Screen.width - buttonWidth) / 2;
            float buttonY = (Screen.height - buttonHeight) / 2;
            Rect buttonRect = new Rect(buttonX, buttonY, buttonWidth, buttonHeight);
            // The DrawButton method returns a boolean value if the button has been clicked or not.
            // This allows us to add logic to our button by putting it in an if statement!
            if (manager.DrawButton(buttonRect, "Click Me!"))
            {
                // DebugConsole is a handwritten console made by Mithril API.
                // We should be able to the button has been clicked in the console.
                // Open the console using `F1`
                DebugConsole.Log("I've been Clicked!");

                // Let's invert the boolean isPopupOpen to draw the popUp!
                isPopupOpen = !isPopupOpen;
            }

            // We can make call a method to draw other GUI elements in.
            popUp();

            // GUIManager has a bunch of different methods for drawing GUI! Such as:
            //manager.DrawCheckBox(new Rect(0, 0, 32, 32), "Check Box", toggleBoolean);
            //manager.DrawRadioButton(new Rect(0, 0, 32, 32), "Radio Button", toggleBoolean);
            //manager.DrawLargeTextCenteredWhite(new Rect(0, 0, 32, 32), "Large White Centered Text");
            //manager.DrawLine(new Rect(0, 0, 32, 32), Color.red);
            //manager.DrawProgressBar(new Rect(0, 0, 32, 32), percentValue, showBGBoolean);
            //manager.DrawUrgentNotification(new Rect(0, 0, 32, 32), "URGENT!", Color.red);
            // And more!
        }

        /// <summary>
        /// You can in fact call GUI method from inside a different method.
        /// However, the method has to be called in OnGUI for it to draw.
        /// </summary>
        /// <returns></returns>
        private void popUp()
        {
            float popUpWidth = 200;
            float popUpHeight = 32;
            float popUpX = (Screen.width - popUpWidth) / 2;
            float popUpY = ((Screen.height - popUpHeight) / 2) + 42;
            Rect popUpRect = new Rect(popUpX, popUpY, popUpWidth, popUpHeight);

            // Let's call this method in order to make a popup happen!
            if (isPopupOpen) manager.DrawUrgentNotification(popUpRect, "Clicked!", Color.red);
        }
    }
}

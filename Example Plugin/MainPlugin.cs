﻿using Timber_and_Stone.API.Event;
using Timber_and_Stone.API;
using System;
using System.Reflection;
using Timber_and_Stone.Event;

namespace Example_Plugin
{
    /// <summary>
    /// The main entry point into developing our mod.
    /// </summary>
    public class MainPlugin : CSharpPlugin, IEventListener
    {
        public override string ModDesc()
        {
            return "An Example Mod.";
        }

        public override string ModName()
        {
            // Can use other methods to give mod name.
            // Assebly.GetExectutingAssembly().GetName();
            return "Example Mod";
        }

        public override Version ModVersion()
        {
            // Get the version from the Assembly
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        // Donesn't really work well
        public override void OnDisable(){}

        // This is fired first before OnLoad().
        public override void OnEnable()
        {
            // Register our plugin in the event manager.
            EventManager.getInstance().Register(this);
        }

        // Useful for loading settings.
        public override void OnLoad()
        {
            // Create a instance of our PluginManager class.
            new PluginManager();
        }

        /// <summary>
        /// Use this instead of OnDisable.
        /// Used to save configs / settings.
        /// </summary>
        ~MainPlugin()
        {
        }
    }
}

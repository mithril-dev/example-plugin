﻿using Example_Plugin.MainGUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Example_Plugin
{
    public class PluginManager
    {
        public static GameObject PluginObject { get; protected set; }

        public PluginManager()
        {
            PluginObject = new GameObject();
            AddMono();
        }

        /// <summary>
        /// Load our MonoBehaviors here.
        /// These can be GUI, or any component you can slap onto a GameObject.
        /// </summary>
        private void AddMono()
        {
            AddMono<GUIExample>();
        }

        private void AddMono<T>() where T : Component
        {
            PluginObject.AddComponent(typeof(T));
        }

    }
}
